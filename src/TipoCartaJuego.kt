enum class TipoCartaJuego {
    CAMBIO_COLOR,
    SUMAR_CARTAS_2,
    SUMAR_CARTAS_4,
    BLOQUEO,
    CAMBIO_SENTIDO,
    NUMERO,
    SIN_TIPO
}
