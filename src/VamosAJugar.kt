import kotlin.random.Random

class VamosAJugar {



    companion object {
        val listaJugadores = listOf<Jugador>(Jugador("Marina"),
                Jugador("Juan"))
        var finalizado = false

        @JvmStatic
        fun main(args: Array<String>) {
            val juego = Juego()

            //repartir las cartas a los jugadores
            listaJugadores.forEach {
                for (i in 0..7) {
                    it.listaDeCartasEnMano.add(juego.robar())
                }
            }

            //TODO dejar al jugardor elegir que carta quiere tirar
            var turnoJugador = Random.nextInt(listaJugadores.size)
            while(!finalizado){
                listaJugadores[turnoJugador]
                printAllPlayersCards()
            }
        }

        //TODO calculara que jugador le toca después de tirar
        private fun siguienteJugador(sentido:Sentido):Int{
            return 1
        }

        /**
         * muestra todas las cartas de todos los jugadores (consola)
         */
        private fun printAllPlayersCards() {
            listaJugadores.forEach{
                print("Cartas de ${it.nombre}: ")
                var cartasAPalabra = "\n"
                var separador = "   "
                it.listaDeCartasEnMano.forEach{carta ->
                    cartasAPalabra = cartasAPalabra.plus("${separador}color:${carta.color}, tipo:${carta.tipo}, number: ${carta.numero}\n")
                }
                print(cartasAPalabra)
                println()
            }
        }
    }


}