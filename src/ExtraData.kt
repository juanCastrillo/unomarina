enum class Color {
    BLUE,
    RED,
    GREEN,
    YELLOW,
    BLACK,
    NO_COLOR
}

enum class Sentido {
    STOPPED,
    CONTINUE,
    CHANGE_DIRECTION
}