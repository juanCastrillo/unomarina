class Juego {

    var color:Color = Color.NO_COLOR

    val cartas:MutableList<Carta>
    private val mazo:Mazo

    constructor(){
        cartas = mutableListOf()
        mazo = Mazo()
        mazo.barajear()
    }

    fun ultimaCarta():Carta {
        return cartas[cartas.size - 1]
    }

    //TODO manejar si el mazo se queda sin cartas
    fun robar():Carta {
        if(!mazo.puedoRobar()) mazo.meterCartas(cartas)
        return mazo.robar()
    }

    fun ponerCarta(carta:Carta, color:Color = carta.color):Sentido {

        //cambiar la carta en juego
        if(cartaAceptable(carta)){
            cartas.add(carta)
            this.color = color
        }

        //devolver el sentido en el que avanza el juego
        return when {
            TipoCartaJuego.CAMBIO_SENTIDO in carta.tipo -> Sentido.CHANGE_DIRECTION
            TipoCartaJuego.BLOQUEO in carta.tipo -> Sentido.STOPPED
            else -> Sentido.CONTINUE
        }
    }

    private fun cartaAceptable(carta:Carta): Boolean {

        //en caso de que nunca hayas tirado una carta
        return if(color == Color.NO_COLOR) true

        //Si el color de la carta es negro siempre vale
        else if(carta.color == Color.BLACK){

            //excepto si no existe esta combinacion de tipo y carta
            !carta.tipo.contains(TipoCartaJuego.CAMBIO_COLOR)
        }

        //comprobamos si el color es el adecuado
        else if(carta.color == color) true

        //Si la carta es del tipo adecuado
        else if(ultimaCarta()!!.tipo == carta.tipo){

            //si la carta es numero hay que comprobar que sea igual
            if(ultimaCarta().tipo.contains(TipoCartaJuego.NUMERO)) {
                ultimaCarta().numero == carta.numero
            }
            else false
        }
        else false
    }
}