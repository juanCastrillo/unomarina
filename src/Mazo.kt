import java.util.*

class Mazo {
    fun numeroDeCartas():Int = cartas.size
    var cartas:MutableList<Carta>
    
    constructor(){

        cartas = mutableListOf()

        //val cartas:
        val listaColores:List<Color> =  listOf(Color.BLACK, Color.BLUE, Color.GREEN, Color.RED, Color.YELLOW)
        for(i in 0..4){

            if(listaColores[i] == Color.BLACK) {
                for (x in 0..1)
                    for (z in 0..3){
                        cartas.add(
                                Carta(listOf(TipoCartaJuego.SUMAR_CARTAS_4, TipoCartaJuego.CAMBIO_COLOR), null, listaColores[i])
                        )
                        cartas.add(
                                Carta(listOf(TipoCartaJuego.CAMBIO_COLOR), null, listaColores[i])
                        )
                    }
            }
            else {
                //Inicializar cartas de numero
                for(j in 0..10) cartas.add(Carta(listOf(TipoCartaJuego.NUMERO), j, listaColores[i]) )

                //1x +2, Bloqueo, CambioSentido
                cartas.add( Carta(listOf(TipoCartaJuego.SUMAR_CARTAS_2), null, listaColores[i]))
                cartas.add( Carta(listOf(TipoCartaJuego.BLOQUEO), null, listaColores[i]))
                cartas.add( Carta(listOf(TipoCartaJuego.CAMBIO_SENTIDO), null, listaColores[i]))
            }

        }
    }

    fun barajear() { cartas.shuffle() }

    fun robar():Carta {
        val posicion = numeroDeCartas()-1
        val carta = cartas[posicion]
        cartas.removeAt(posicion)
        return carta
    }

    fun puedoRobar():Boolean = cartas.size >= 1

    fun meterCartas(cartas:MutableList<Carta>){
        this.cartas = cartas
        barajear()
    }
}


