class Jugador {

    val nombre:String
    val listaDeCartasEnMano:MutableList<Carta>
    fun numeroCartas() = listaDeCartasEnMano.size

    constructor(nombre:String = "") {
        this.nombre = nombre
        listaDeCartasEnMano = mutableListOf()
    }
}